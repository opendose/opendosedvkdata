# OpenDose 3D voxel dose kernel data files

This is the reporitory of voxel dose kernel (VDK) data files for the project [OpenDose3D](http://gitlab.com/opendose/opendose3d).
This is a static repository, just for downloading the files. If you need more files like this please use the scripts available at opendose3d project. The offered data is for most common radionuclides used in clinical practice.