import sys
import time
import math
from pathlib import Path
import json
from json_utils import json_zip, json_unzip
import logging
import types
import multiprocessing

def log_newline(self, how_many_lines=1):
    # Switch handler, output a blank line
    self.removeHandler(self.console_handler)
    self.addHandler(self.blank_handler)
    for i in range(how_many_lines):
        self.info('')

    # Switch back
    self.removeHandler(self.blank_handler)
    self.addHandler(self.console_handler)

def human_time(t):
    # Function to get the human readable time instead of the number of seconds
    units = [('day', 86400), ('hour', 3600), ('minute', 60), ('second', 1)]
    parts = []
    secs = t
    for unit, mul in units:
        if secs / mul >= 1 or mul == 1:
            if mul > 1:
                n = int(math.floor(secs / mul))
                secs -= n * mul
            else:
                n = secs if secs != int(secs) else int(secs)
            parts.append('{:d} {}{}'.format(
                int(math.floor(n)), unit, '' if n == 1 else 's'))
    return ', '.join(parts)+', {} msec'.format(int(math.floor(secs*1000))-1000*int(math.floor(secs)))

def create_logger(loggerName):
    # Create a handler
    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.DEBUG)
    console_handler.setFormatter(logging.Formatter(fmt="\x1b[80D\x1b[1A\x1b[K%(name)s %(levelname)-8s: %(message)s"))

    # Create a "blank line" handler
    blank_handler = logging.StreamHandler()
    blank_handler.setLevel(logging.DEBUG)
    blank_handler.setFormatter(logging.Formatter(fmt=''))

    # Create a logger, with the previously-defined handler
    logger = logging.getLogger(loggerName)
    logger.setLevel(logging.DEBUG)
    logger.addHandler(console_handler)

    # Save some data and add a method to logger object
    logger.console_handler = console_handler
    logger.blank_handler = blank_handler
    logger.newline = types.MethodType(log_newline, logger)

    return logger


def getScriptPath():
    try:
        script_path = Path(__file__).resolve().parent
    except:
        from inspect import getsourcefile
        script_path = Path(getsourcefile(lambda: 0)).resolve().parent
    finally:
        return script_path

def compressFile(fileName):
    with open(str(fileName), 'r') as fr:
        js = json.load(fr)
        jsc = json_zip(js, method='zip')
        # jscg = json_zip(js, method='gzip') # Compress again with gzip
        # if len(jscg['base64(gzip(o))'])<len(jsc['base64(zip(o))']):
        #     jsc = jscg  # Select the best compression method
        newFileName = str(fileName)+".zip"  # add .zip
        with open(newFileName, 'w') as fw:
            fw.write(json.dumps(jsc))
    fileName.unlink()


def compressDataBase():
    logger = create_logger('compressing')
    logger.newline()
    DBdirectory = getScriptPath() / "DVK"
    filenames = list(DBdirectory.glob('DVK_*.json'))
    numfiles=len(filenames)
    start = time.time()
    with multiprocessing.Pool() as p:
        for index,filename in enumerate(p.imap(compressFile, filenames)):
            logger.info(f"Processing {filenames[index]} - {100*index/numfiles:.1f}%")
    end = time.time()
    logger.info(f"Elapsed time = {human_time(end-start)}")

def decompressFile(fileName):
    with open(str(fileName), 'r') as fr:
        jsc = json.load(fr)
        js = dict(json_unzip(jsc))
        while js!=jsc:
            jsc = js
            js = dict(json_unzip(jsc, insist=False))
        newFileName = '.'.join(str(fileName).split('.')[
            :-1])  # Remove the .zip
        with open(newFileName, 'w') as fw:
            fw.write(json.dumps(js, indent=4, sort_keys=True))
    fileName.unlink()

def decompressDataBase():
    logger = create_logger('decompressing')
    logger.newline()
    DBdirectory = getScriptPath() / "DVK"
    filenames = list(DBdirectory.glob('DVK_*.json.zip'))
    numfiles=len(filenames)
    start = time.time()
    with multiprocessing.Pool() as p:
        for index,filename in enumerate(p.imap(decompressFile, filenames)):
            logger.info(f"Processing {filenames[index]} - {100*index/numfiles:.1f}%")
    end = time.time()
    logger.info(f"Elapsed time = {human_time(end-start)}")

def isCompressed():
    DBdirectory = getScriptPath() / "DVK"
    filenames = list(DBdirectory.glob('DVK_*.json'))
    return False if filenames else True