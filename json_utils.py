# RA, 2019-01-22
# Compress and decompress a JSON object

# License: CC0 -- "No rights reserved"

# For zlib license, see https://docs.python.org/3/license.html
import zlib
import gzip
import json, base64

ZIPJSON_KEY = 'base64(zip(o))'
GZIPJSON_KEY = 'base64(gzip(o))'

def json_zip(j, method='zip'):
    if method=='zip':
        j = {
            ZIPJSON_KEY: base64.b64encode(
                zlib.compress(
                    json.dumps(j).encode('utf-8')
                )
            ).decode('ascii')
        }
    elif method=='gzip':
        j = {
            GZIPJSON_KEY: base64.b64encode(
                gzip.compress(
                    json.dumps(j).encode('utf-8')
                )
            ).decode('ascii')
        }
    else:
        raise RuntimeError("Wrong or unsupported compression method, select between zip and gzip")

    return j


def json_unzip(j, insist=True):
    if not isinstance(j, dict):
        if insist:
            raise RuntimeError(f"JSON not in the expected format: \n {ZIPJSON_KEY}: zipstring \n {GZIPJSON_KEY}: gzipstring")
        else:
            return j
    if ZIPJSON_KEY in j:
        try:
            j = zlib.decompress(base64.b64decode(j[ZIPJSON_KEY]))
        except Exception as e:
            raise RuntimeError(f"Could not decode/unzip the contents \n {e}")
    elif GZIPJSON_KEY in j:
        try:
            j = gzip.decompress(base64.b64decode(j[GZIPJSON_KEY]))
        except Exception as e:
            raise RuntimeError(f"Could not decode/unzip the contents \n {e}")
    else:
        if insist:
            raise RuntimeError(f"JSON not in the expected format: \n {ZIPJSON_KEY}: zipstring \n {GZIPJSON_KEY}: gzipstring")
        else:
            return j

    try:
        j = json.loads(j)
    except Exception as e:
        raise RuntimeError(f"Could not interpret the unzipped contents \n {e}")

    return j


import unittest

class TestJsonZipMethods(unittest.TestCase):
    # Unzipped
    unzipped = {'a': "A", 'b': "B"}

    # Zipped
    zipped = {ZIPJSON_KEY: "eJyrVkpUslJQclTSUVBKArGclGoBLeoETw=="}
    gzipped = {GZIPJSON_KEY: "H4sIAGJZa18C/6tWSlSyUlByVNJRUEoCsZyUagEqwhVWFAAAAA=="}

    # List of items
    items = [123, "123", unzipped]

    def test_json_zip(self):
        print(self.zipped, json_zip(self.unzipped))
        self.assertEqual(self.zipped, json_zip(self.unzipped))

    def test_json_unzip(self):
        print(self.unzipped, json_unzip(self.zipped))
        self.assertEqual(self.unzipped, json_unzip(self.zipped))

    def test_json_zipunzip(self):
        self.assertEqual(self.unzipped, json_unzip(json_zip(self.unzipped)))

    def test_json_ungzip(self):
        self.assertEqual(self.unzipped, json_unzip(self.gzipped))

    def test_json_gzipungzip(self):
        self.assertEqual(self.unzipped, json_unzip(json_zip(self.unzipped, method='gzip')))

    def test_json_zipunzip_chinese(self):
        item = {'hello': "你好"}
        self.assertEqual(item, json_unzip(json_zip(item)))

    def test_json_gzipungzip_chinese(self):
        item = {'hello': "你好"}
        self.assertEqual(item, json_unzip(json_zip(item, method='gzip')))

    def test_json_unzip_insist_failure(self):
        for item in self.items:
            with self.assertRaises(RuntimeError):
                json_unzip(item, insist=True)

    def test_json_unzip_noinsist_justified(self):
        for item in self.items:
            self.assertEqual(item, json_unzip(item, insist=False))

    def test_json_unzip_noinsist_unjustified(self):
        self.assertEqual(self.unzipped, json_unzip(self.zipped, insist=False))

    def test_json_ungzip_noinsist_unjustified(self):
        self.assertEqual(self.unzipped, json_unzip(self.gzipped, insist=False))


if __name__ == '__main__':
    unittest.main()